const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());

const dataFilePath = 'app.json';

// Read data from file
function readDataFromFile() {
  try {
    const data = fs.readFileSync(dataFilePath, 'utf8');
    return JSON.parse(data);
  } catch (error) {
    console.error('Error reading data from file:', error);
    return []; // Return an empty array if there is an error reading the file
  }
}

// Write data to file
function writeDataToFile(data) {
  try {
    fs.writeFileSync(dataFilePath, JSON.stringify(data, null, 2), 'utf8');
  } catch (error) {
    console.error('Error writing data to file:', error);
    // Handle the error if needed, e.g., log it or return an error response
  }
}

// Endpoint to insert data
app.post('/insert', (req, res) => {
  const newData = req.body;

  if (!newData || Object.keys(newData).length === 0) {
    return res.status(400).json({ error: 'Invalid data format' });
  }

  const allData = readDataFromFile();
  allData.push(newData);
  writeDataToFile(allData);

  res.json({ message: 'Data inserted successfully', data: newData });
});

// Endpoint to fetch data
app.get('/getData', (req, res) => {
  const allData = readDataFromFile();
  res.json({ data: allData });
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

